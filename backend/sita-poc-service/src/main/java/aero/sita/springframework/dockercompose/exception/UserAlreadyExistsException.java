package aero.sita.springframework.dockercompose.exception;

public class UserAlreadyExistsException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = -3677209703227058039L;
    private String message;

    public UserAlreadyExistsException(String message) {
        super(message);
        this.message = message;
    }

    public UserAlreadyExistsException() {
    }
}