package aero.sita.springframework.dockercompose.exception;

public class UserIdConflictException extends Exception {
    /**
     *
     */
    private String message;

    public UserIdConflictException(String message) {
        super(message);
        this.message = message;
    }

    public UserIdConflictException() {
    }
}