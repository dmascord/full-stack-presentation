package aero.sita.springframework.dockercompose.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "database_sequences")
/**
 * Lombok annotation for generating getters and setters
 */
@Getter
@Setter
/**
 * Lombok annotation for generating default constructor
 */
@AllArgsConstructor
/**
 * Lombok annotation for generating parameterized constructor
 */
@NoArgsConstructor
public class DatabaseSequence {
 
    @Id
    private String id;
 
    private int seq;
 
}