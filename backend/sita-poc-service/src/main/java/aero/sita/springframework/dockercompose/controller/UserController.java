package aero.sita.springframework.dockercompose.controller;

import aero.sita.springframework.dockercompose.domain.User;
import aero.sita.springframework.dockercompose.exception.UserAlreadyExistsException;
import aero.sita.springframework.dockercompose.exception.UserIdConflictException;
import aero.sita.springframework.dockercompose.service.UserService;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



/**
* RestController annotation is used to create
* Restful web services using Spring MVC
*/
@RestController

/**
* RequestMapping annotation maps
* HTTP requests to handler methods
*/
@RequestMapping(value = "/api/v1/")
public class UserController {

  @Value("${app.userIdConflictexceptionMessage}")
  private String exceptionMessage;

  private UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("users")
  public Collection<User> users() {
        return userService.getAllUsers();
  }

  @GetMapping("users/{id}")
    ResponseEntity<?> getGroup(@PathVariable Integer id) {
        Optional<User> group = userService.getUser(id);
        return group.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

  /**
  * PostMapping Annotation for mapping HTTP POST requests onto
  * specific handler methods.
  */
  @PostMapping("user")
  public ResponseEntity<?> saveUser(@RequestBody User user) throws UserAlreadyExistsException, Exception {
    User savedUser = userService.saveUser(user);
    ResponseEntity responseEntity = new ResponseEntity(savedUser, HttpStatus.CREATED);
    return responseEntity;
  }

  @PutMapping("user/{id}")
  public ResponseEntity<?> saveUser(@RequestBody User user, @PathVariable Integer id) throws UserAlreadyExistsException, Exception {
    if (id > 0 && user.getId() != id )
    {
      throw new UserIdConflictException(exceptionMessage);
    }
    User savedUser = userService.updateUser(user);
    ResponseEntity responseEntity = new ResponseEntity(savedUser, HttpStatus.CREATED);
    return responseEntity;
  }

  @DeleteMapping("user/{id}")
  public ResponseEntity<?> deleteUser(@PathVariable Integer id) throws UserAlreadyExistsException, Exception {
    userService.deleteUserById(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping("users")
  public ResponseEntity<?> deleteAllUser() throws UserAlreadyExistsException, Exception {
    userService.deleteAllUsers();
    return new ResponseEntity<>(HttpStatus.OK);
  }
}