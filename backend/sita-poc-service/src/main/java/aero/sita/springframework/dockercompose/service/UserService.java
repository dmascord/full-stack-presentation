package aero.sita.springframework.dockercompose.service;

import java.util.Collection;
import java.util.Optional;

import aero.sita.springframework.dockercompose.domain.User;
import aero.sita.springframework.dockercompose.exception.UserAlreadyExistsException;

public interface UserService {

    /**
     * AbstractMethod to save a user
     */
    public User saveUser(User user) throws UserAlreadyExistsException;
    public User updateUser(User user);
    public void deleteUserById(int userId);
    public void deleteAllUsers();
    public Collection<User> getAllUsers();
    public Optional<User> getUser(Integer userId);
}