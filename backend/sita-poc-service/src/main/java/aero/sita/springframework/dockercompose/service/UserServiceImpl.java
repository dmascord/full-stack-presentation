package aero.sita.springframework.dockercompose.service;

import aero.sita.springframework.dockercompose.domain.User;
import aero.sita.springframework.dockercompose.exception.UserAlreadyExistsException;
import aero.sita.springframework.dockercompose.repository.UserRepository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @Service indicates annotated class is a service which holds business logic in
 *          the Service layer
 */
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    /*
     * to get property value
     */
    @Value("${app.exceptionMessage}")
    private String exceptionMessage;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /*
     * save a user in dockerized mongo container
     */
    @Override
    public User saveUser(final User user) throws UserAlreadyExistsException {
        /*
         * Implementation of saveBlog method Throw UserAlreadyExistsException if user id
         * already exists in db
         */
        if (user.getId() > 0 && userRepository.existsById(user.getId())) {
            throw new UserAlreadyExistsException(exceptionMessage);
        } else {
            return userRepository.save(user);
        }   
    }

    @Override
    public User updateUser(final User user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteAllUsers() {
        userRepository.deleteAll();
    }

    @Override
    public void deleteUserById(final int userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public Collection<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> getUser(Integer userId) {
        return userRepository.findById(userId);
    }
}